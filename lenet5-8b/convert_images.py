#!/bin/python

# This script converts C-style array of images into csv data files

in_file = "lenet/int8_t_images.c"

fi = open(in_file, "r")


# Skip file contents until a specified keyword is found
def reach_line(line) :
	for l in fi :
		if l.find(line) >= 0 : break
	# End of function

# Import lines of expected comma-separated numbers
# Drop characters {}
def process_images(out_file) :
	fo = open(out_file, "w")

	for l in fi :
		l = l.replace("{", "")
		l = l.replace("}", "")
		l = l.replace(";", "")
		l = l.split('//')[0]
		l = l.strip()
		l = l.lstrip(',')
		if l == '' : continue
		fo.write(l + '\n')

	# End of function


# Launch conversion of the file

reach_line("test_mnist[][32][32][1]")
process_images("input.csv")


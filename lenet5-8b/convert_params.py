#!/bin/python

# This script converts C-style parameters into csv data files


# For ceil/floor
import math



# Variables for input data
in_file = "lenet/int8_t_parameters.h"

fi = open(in_file, "r")



# Re-quantization objectives

max_absw  = 255
max_scale = 63

# Miscellaneous shared variables

weight_rescaled = []

zero_point = 0

norm_bias  = []
norm_scale = []
norm_shr   = []



# Skip file contents until a specified keyword is found
def reach_line(line) :
	for l in fi :
		if l.find(line) >= 0 : break
	# End of function

# Get the zero_point value
def get_zero_point() :
	l = fi.readline()
	l = l.replace("{", "")
	l = l.replace("}", "")
	l = l.replace(",", "")
	l = l.replace(";", "")
	l = l.strip()
	zero_point = 0
	if l == '' : return
	zero_point = int(l)
	print("Zero point found : ", zero_point)

# Get the pairs {scale, shift} for a set of neurons
def get_biases(nlines) :
	global norm_bias
	acc = []
	for i in range(nlines) :
		l = fi.readline()
		l = l.replace("{", "")
		l = l.replace("}", "")
		l = l.replace(";", "")
		l = l.strip()
		l = l.split(',')
		l = [ v for v in l if v != '' ]
		acc = acc + l
	norm_bias = [ int(v) for v in acc ]

# Get the pairs {scale, shift} for a set of neurons
def get_scales(nlines) :
	global norm_scale
	global norm_shr
	l = fi.readline()
	l = l.replace("{", "")
	l = l.replace("}", "")
	l = l.replace(";", "")
	l = l.strip()
	l = l.split(',')
	#l = filter(lambda v: v != "", l)
	l = [ v for v in l if v != '' ]
	norm_scale = [ int(l[2*i+0]) for i in range(nlines) ]
	norm_shr   = [ int(l[2*i+1]) for i in range(nlines) ]

# Import lines of expected comma-separated numbers
# Drop characters {}
def process_weights(out_file, neuNum, neuSize) :
	fo = open(out_file, "w")

	global max_absw
	global weight_rescaled
	weight_rescaled = []

	# FIXME Rescaling requires to load all weights and concatenate lines to manipulate full kernels

	for n in range(neuNum) :
		arr = []

		for l in fi :
			l = l.replace("{", "")
			l = l.replace("}", "")
			l = l.replace(";", "")
			l = l.strip()
			if l == '' : continue

			# Convert into array of integers, to manipulate number of bits
			arrLoc = l.split(',')
			arrLoc = [ int(v) for v in arrLoc if v != '' ]
			if len(arrLoc) == 0 : continue

			# Append to current neuron
			arr.extend(arrLoc)

			if len(arr) >= neuSize : break

		# Apply rescaling to the complete kernel
		minw = min(arr)
		maxw = max(arr)
		absw = max([abs(minw), abs(maxw)])
		#print("Weights : range ", minw, " to ", maxw, ', bits ', math.ceil(math.log2(absw)) + 1)
		# Fill the shared array of weight rescaling
		rescale = 1;
		if absw > max_absw :
			rescale = absw / max_absw
			arr = [ int(v / rescale) for v in arr ]
			# Truncate largest values in case rounding did exceed the desired max
			arr = [ max([min([v, max_absw]), -max_absw]) for v in arr ]
		weight_rescaled.append(rescale)

		#minw = min(arr)
		#maxw = max(arr)
		#absw = max([abs(minw), abs(maxw)])
		#print("Weights : range ", minw, " to ", maxw, ', bits ', math.ceil(math.log2(absw)) + 1, ', rescale ', rescale)

		#arr = [ format(v, "%i") for v in arr ]
		arr = [ str(v) for v in arr ]
		#print(arr)
		fo.write(','.join(arr) + '\n')

		#print("Weights rescaling : ", weight_rescaled)

	# End of function

# Write NORM config as csv
def write_norm(out_file, nlines) :
	fo = open(out_file, "w")

	global max_scale

	global weight_rescaled

	global norm_bias
	global norm_scale
	global norm_shr

	# Take into account the additional implicit shr by 31 bits
	# See function tflite_fixmul() from C code
	norm_shr = [ v + 31 for v in norm_shr ]

	# Count the common factors of 2 in the scale values, in order to move as many bits as possible to the shr operation
	min_fac2_in_scales = []
	for i in range(nlines) :
		s = norm_scale[i]
		fac2 = 0
		while (s > 0) and (s % 2 == 0) :
			s = s / 2
			fac2 += 1
		min_fac2_in_scales.append(fac2)
	min_fac2_in_scales = min(min_fac2_in_scales)
	print("Min factors of 2 in scales : ", min_fac2_in_scales)

	# Incorporate the weight rescaling here
	for i in range(nlines) :
		if weight_rescaled[i] != 1 :
			norm_scale[i] *= weight_rescaled[i]
			norm_bias[i]  *= weight_rescaled[i]

	# Reduce the number of significant digits of the scale, also reduce the shr value
	for i in range(nlines) :
		while norm_scale[i] > max_scale :
			norm_scale[i] /= 2
			norm_shr[i] -= 1

	# Incorporate the output zero point (-128) into the bias
	# Add (128 << shr) / Scale
	#for i in range(nlines) :
	#	inc = (128 << norm_shr[i]) / norm_scale[i]
	#	print("Bias ", norm_bias[i], " + ", inc)
	#	norm_bias[i] += int(inc)

	# Convert back to integer (float number may be introduced by weight_rescaled)
	norm_scale = [ int(v) for v in norm_scale ]
	norm_bias  = [ int(v) for v in norm_bias ]

	for i in range(nlines) :
		fo.write(str(norm_bias[i]) + ', ' + str(norm_scale[i]) + ', ' + str(norm_shr[i]) + '\n')
	# End of function



# Launch conversion of the file

# Neu0

reach_line("C1_zero_points_in")
get_zero_point()

reach_line("C1_kernels[6][5][5][1]")
process_weights("neu0.csv", 6, 25)

reach_line("C1_biases[6]")
get_biases(1)
reach_line("C1_m0_s[6]")
get_scales(6)
write_norm("norm0.csv", 6)

# Neu1

reach_line("C3_zero_points_in[1]")
get_zero_point()

reach_line("C3_kernels[16][5][5][6]")
process_weights("neu1.csv", 16, 150)

reach_line("C3_biases[16]")
get_biases(2)
reach_line("C3_m0_s[16]")
get_scales(16)
write_norm("norm1.csv", 16)

# Neu2

reach_line("F5_zero_points_in")
get_zero_point()

reach_line("F5_weights[120][400]")
process_weights("neu2.csv", 120, 400)

reach_line("F5_biases[120]")
get_biases(8)
reach_line("F5_m0_s[1]")
get_scales(1)
norm_scale = [ norm_scale[0] for i in range(120) ]
norm_shr   = [ norm_shr[0]   for i in range(120) ]
write_norm("norm2.csv", 120)

# Neu3

reach_line("F6_zero_points_in[1]")
get_zero_point()

reach_line("F6_weights[84][120]")
process_weights("neu3.csv", 84, 120)

reach_line("F6_biases[84]")
get_biases(6)
reach_line("F6_m0_s[1]")
get_scales(1)
norm_scale = [ norm_scale[0] for i in range(84) ]
norm_shr   = [ norm_shr[0]   for i in range(84) ]
write_norm("norm3.csv", 84)

# Neu4

reach_line("F7_zero_points_in")
get_zero_point()

reach_line("F7_weights[10][84]")
process_weights("neu4.csv", 10, 84)

reach_line("F7_biases[10]")
get_biases(1)



CFLAGS  = -Wall -std=gnu99
LDFLAGS = -lm

ifdef DEBUG
	CFLAGS  += -g -O0
	LDFLAGS +=
else
	CFLAGS  += -march=native -O2 -DNDEBUG
	LDFLAGS += -s
endif

all:
	$(MAKE) gen-config
	$(MAKE) scale-dataset


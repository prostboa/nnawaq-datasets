
import scipy.io
import numpy as np

import sys

if len(sys.argv) < 3:
	print("Arguments: " + sys.argv[0] + " <filename> <nimg>")
	print("  If <nimg> is zero, the entire file is processed")
	sys.exit(1)

fname      = sys.argv[1]
param_nimg = int(sys.argv[2])

mat = scipy.io.loadmat(fname)

data_img   = mat['X']
data_class = mat['y']

dimx     = len(data_img)
dimy     = len(data_img[0])
dimcolor = len(data_img[0][0])
nimg     = len(data_img[0][0][0])

print("There are " + str(nimg) + " images " + str(dimx) + "x" + str(dimy) + "x" + str(dimcolor))

if (param_nimg > 0) and (param_nimg < nimg): nimg = param_nimg
print("Processing " + str(nimg) + " images")

print("Writing images as CSV data...")

fo = open(fname + '.img.csv', 'w')

for i in range(0, nimg):

	for c in range(0, dimcolor):
		for x in range(0, dimx):
			for y in range(0, dimy):
				fo.write(str(data_img[x][y][c][i]) + ',')

		fo.write("\n")

fo.close()

print("Writing images as 12b/px CSV data...")

fo = open(fname + '.img12b.csv', 'w')

for i in range(0, nimg):

	for c in range(0, dimcolor):
		for sh in range(0, 4):
			for x in range(0, dimx):
				for y in range(0, dimy):
					val = data_img[x][y][c][i]
					val = ((val << sh) & 0x80) >> 7
					fo.write(str(val) + ',')

			fo.write("\n")

fo.close()

print("Writing classes as CSV data...")

fo = open(fname + '.class.txt', 'w')

for i in range(0, nimg):
	c = data_class[i][0]
	if c == 10: c = 0
	fo.write(str(c) + "\n")

fo.close()


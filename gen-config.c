
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_usage(int argc, char** argv) {
	printf("Usage:\n");
	printf("  %s [options]\n", argv[0]);
	printf("\n");
	printf("Options:\n");
	printf("  -u -h --help      Display this help and exit\n");
	printf("  -n <n>            Base number of neurons for VGG\n");
	printf("  -f <x> <y> <z>    Image size\n");
	printf("  -dw <b>           Data width\n");
	printf("  -fn <n>           Number of frames\n");
	printf("\n");
}

int main(int argc, char** argv) {
	char buf[1024];
	FILE* F;

	unsigned n  = 64;
	unsigned fx = 32;
	unsigned fy = 32;
	unsigned fz = 3;
	unsigned fn = 200;
	unsigned dw = 8;

	// Parse command-line arguments

	unsigned argi = 1;
	do {
		if(argi >= argc) break;
		char* arg = argv[argi];
		if(arg[0]==0) break;

		char* getparam_str() {
			argi++;
			if(argi >= argc) {
				printf("Error: Missing parameters after '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
			return argv[argi];
		}

		if(strcmp(arg, "-u")==0 || strcmp(arg, "-h")==0 || strcmp(arg, "--help")==0) {
			print_usage(argc, argv);
			exit(EXIT_SUCCESS);
		}

		else if(strcmp(arg, "-n")==0) {
			n = atoi(getparam_str());
		}
		else if(strcmp(arg, "-f")==0) {
			fx = atoi(getparam_str());
			fy = atoi(getparam_str());
			fz = atoi(getparam_str());
		}
		else if(strcmp(arg, "-dw")==0) {
			dw = atoi(getparam_str());
		}
		else if(strcmp(arg, "-fn")==0) {
			fn = atoi(getparam_str());
		}

		else {
			printf("Error: Unknown parameter '%s'\n", arg);
			exit(EXIT_FAILURE);
		}

		// Here the parameter was known. Increment index.
		argi++;

	} while(1);

	// Prepare constants

	const unsigned neu_layers_id[] = {
		1, 2,
		4, 5,
		7, 8,
		11, 12, 13
	};
	const unsigned neu_layers_nb = 9;

	unsigned neu_fsize[] = {
		9*fz, 9*n,
		9*n, 9*2*n,
		9*2*n, 9*4*n,
		4*4*4*n, 8*n, 8*n
	};
	unsigned neu_nb[] = {
		n, n,
		2*n, 2*n,
		4*n, 4*n,
		8*n, 8*n, 100
	};

	// Generate the config of neuron & recode layers

	for(unsigned i=0; i<neu_layers_nb; i++) {
		unsigned fsize = neu_fsize[i];
		unsigned nbneu = neu_nb[i];

		printf("Layer neu%u, file ID %u: fsize %u nbneu %u\n", i, neu_layers_id[i], fsize, nbneu);

		sprintf(buf, "weights%u.csv", neu_layers_id[i]);
		F = fopen(buf, "wb");
		for(unsigned nidx=0; nidx<nbneu; nidx++) {
			for(unsigned f=0; f<fsize; f++) {
				int v = rand() % 11 - 5;
				if(v > 0) v = 1;
				else if(v < 0) v = -1;
				fprintf(F, "%i,", v);
			}
			fprintf(F, "\n");
		}
		fclose(F);

		if(i==neu_layers_nb-1) break;

		sprintf(buf, "thresholds%u.csv", neu_layers_id[i]);
		F = fopen(buf, "wb");
		for(n=0; n<nbneu; n++) {
			fprintf(F, "0,0,\n");
		}
		fclose(F);

	}  // Scan all layers

	// Generate the fake frames

	sprintf(buf, "input1.csv");
	F = fopen(buf, "wb");

	for(unsigned f=0; f<fn; f++) {
		for(unsigned i=0; i<fx*fy*fz; i++) {
			int v = rand();

			if(dw==1) {
				v = v % 10;
				if(v != 0) v = 1;
			}
			else if(dw==2) {
				v = v % 11 - 5;
				if(v > 0) v = 1;
				else if(v < 0) v = -1;
			}
			else {
				int range = (1 << (dw-1)) - 1;
				v = v % (2*range+1) - range;
			}

			fprintf(F, "%i,", v);
		}
		fprintf(F, "\n");
	}
	fclose(F);

	return 0;
}


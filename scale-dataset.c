
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

// Create a 2-dimensions array
double** array_create_dim2(unsigned nrows, unsigned ncol) {
	double **array = malloc(nrows * sizeof(*array));
	array[0] = calloc(nrows * ncol, sizeof(**array));
	for(unsigned i=1; i<nrows; i++) array[i] = array[i-1] + ncol;
	return array;
}

double** loadfile_dim2(FILE* F, unsigned nframes, unsigned fsize) {
	double** arr = array_create_dim2(nframes, fsize);

	static size_t linebuf_size = 0;
	static char* linebuf = NULL;
	ssize_t r;

	unsigned idx_frame = 0;
	unsigned idx_val = 0;

	do {

		// Get one line = one frame
		r = getline(&linebuf, &linebuf_size, F);
		// Exit when the end of the file is reached
		if(r < 0) break;

		// Convert all commas to space, to ease parsing
		for(char* ptr = linebuf; *ptr!=0; ptr++) if(*ptr==',') *ptr = ' ';

		// Parse the line
		char* ptr = linebuf;
		do {
			double d = 0;

			while(isspace(*ptr)!=0 && *ptr!=0) ptr++;
			if(*ptr==0) break;

			sscanf(ptr, "%lf", &d);
			arr[idx_frame][idx_val++] = d;
			if(idx_val >= fsize) { idx_frame++; idx_val = 0; }
			if(idx_frame >= nframes) break;

			while(isspace(*ptr)==0 && *ptr!=0) ptr++;
			if(*ptr==0) break;

		} while(1);

		if(idx_frame >= nframes) break;

	} while(1);  // Read the lines of the file

	if(idx_frame < nframes) {
		printf("Warning: Wanted %u frames, only got %u\n", nframes, idx_frame);
		exit(EXIT_FAILURE);
	}

	return arr;
}

void print_usage(int argc, char** argv) {
	printf("Usage:\n");
	printf("  %s [options]\n", argv[0]);
	printf("\n");
	printf("Options:\n");
	printf("  -u -h --help      Display this help and exit\n");
	printf("  -n <n>            Base number of neurons for VGG\n");
	printf("  -f <x> <y> <z>    Image size\n");
	printf("  -dw <b>           Data width\n");
	printf("  -fn <n>           Number of frames\n");
	printf("  -scale <s>        Scale to range -s;+s before saturation\n");
	printf("  -if <file>        Input file\n");
	printf("  -of <file>        Output file\n");
	printf("  -ifth <file>      Input file for thresholds\n");
	printf("  -ofth <file>      Output file for thresholds\n");
	printf("\n");
}

int main(int argc, char** argv) {
	unsigned n  = 64;
	unsigned fx = 32;
	unsigned fy = 32;
	unsigned fz = 3;
	unsigned fn = 200;
	unsigned dw = 8;
	double   param_scale = 0;

	char* filename_in = NULL;
	char* filename_out = NULL;

	char* filename_th_in = NULL;
	char* filename_th_out = NULL;

	// Parse command-line arguments

	unsigned argi = 1;
	do {
		if(argi >= argc) break;
		char* arg = argv[argi];
		if(arg[0]==0) break;

		char* getparam_str() {
			argi++;
			if(argi >= argc) {
				printf("Error: Missing parameters after '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
			return argv[argi];
		}

		if(strcmp(arg, "-u")==0 || strcmp(arg, "-h")==0 || strcmp(arg, "--help")==0) {
			print_usage(argc, argv);
			exit(EXIT_SUCCESS);
		}

		else if(strcmp(arg, "-n")==0) {
			n = atoi(getparam_str());
		}
		else if(strcmp(arg, "-f")==0) {
			fx = atoi(getparam_str());
			fy = atoi(getparam_str());
			fz = atoi(getparam_str());
		}
		else if(strcmp(arg, "-dw")==0) {
			dw = atoi(getparam_str());
		}
		else if(strcmp(arg, "-fn")==0) {
			fn = atoi(getparam_str());
		}
		else if(strcmp(arg, "-scale")==0) {
			param_scale = atof(getparam_str());
		}

		else if(strcmp(arg, "-if")==0) {
			filename_in = getparam_str();
		}
		else if(strcmp(arg, "-of")==0) {
			filename_out = getparam_str();
		}
		else if(strcmp(arg, "-ifth")==0) {
			filename_th_in = getparam_str();
		}
		else if(strcmp(arg, "-ofth")==0) {
			filename_th_out = getparam_str();
		}

		else {
			printf("Error: Unknown parameter '%s'\n", arg);
			exit(EXIT_FAILURE);
		}

		// Here the parameter was known. Increment index.
		argi++;

	} while(1);

	// Open files

	FILE* Fi = NULL;
	FILE* Fo = NULL;
	FILE* Fith = NULL;
	FILE* Foth = NULL;

	if(filename_in!=NULL) {
		Fi = fopen(filename_in, "rb");
		if(Fi==NULL) {
			printf("Error: Can't open input file %s\n", filename_in);
			exit(EXIT_FAILURE);
		}
	}

	if(filename_out!=NULL) {
		Fo = fopen(filename_out, "wb");
		if(Fo==NULL) {
			printf("Error: Can't open output file %s\n", filename_out);
			exit(EXIT_FAILURE);
		}
	}

	if(filename_th_in!=NULL) {
		Fith = fopen(filename_th_in, "rb");
		if(Fith==NULL) {
			printf("Error: Can't open input thresholds file %s\n", filename_th_in);
			exit(EXIT_FAILURE);
		}
	}

	if(filename_th_out!=NULL) {
		Foth = fopen(filename_th_out, "wb");
		if(Foth==NULL) {
			printf("Error: Can't open output thresholds file %s\n", filename_th_out);
			exit(EXIT_FAILURE);
		}
	}

	// Load input files

	unsigned fsize = fx * fy * fz;

	double** arr = NULL;
	if(Fi!=NULL) {
		if(fn==0 || fsize==0) {
			printf("Error: Can't load input data: fn=%u fsize=%u\n", fn, fsize);
			exit(EXIT_FAILURE);
		}
		arr = loadfile_dim2(Fi, fn, fsize);
	}

	double** arrth = NULL;
	if(Fith!=NULL) {
		if(n==0) {
			printf("Error: Can't load thresholds data: n=%u\n", n);
			exit(EXIT_FAILURE);
		}
		arrth = loadfile_dim2(Fith, n, 2);
	}

	// Get the scaling factor

	double range = (1 << (dw - 1)) - 1;

	double scale = param_scale;
	if(scale==0) {
		if(arr == NULL) {
			printf("Error: Can't get scaling factor because no input data\n");
			exit(EXIT_FAILURE);
		}

		double val_min = 0;
		double val_max = 0;

		for(unsigned f=0; f<fn; f++) {
			for(unsigned i=0; i<fsize; i++) {
				double val = arr[f][i];
				if(val < val_min) val_min = val;
				if(val > val_max) val_max = val;
			}
		}

		if(val_min != 0) {
			double loc_scale = range / (-val_min);
			if(scale==0 || loc_scale < scale) scale = loc_scale;
		}
		if(val_max != 0) {
			double loc_scale = range / val_max;
			if(scale==0 || loc_scale < scale) scale = loc_scale;
		}

	}

	if(scale <= 0) {
		printf("Error: Obtained scaling factor %g\n", scale);
		exit(EXIT_FAILURE);
	}

	if(param_scale==0) {
		printf("Scaling factor: %g\n", scale);
	}

	// Scale and save

	if(Fo!=NULL) {
		if(arr == NULL) {
			printf("Error: Can't generate output data because no input data\n");
			exit(EXIT_FAILURE);
		}
		// Apply scaling
		if(scale != 1) {
			for(unsigned f=0; f<fn; f++) {
				for(unsigned i=0; i<fsize; i++) {
					double val = arr[f][i] * scale;
					if(val < -range) val = -range;
					if(val > range)  val = range;
					arr[f][i] = val;
				}
			}
		}
		// Write output
		for(unsigned f=0; f<fn; f++) {
			unsigned i = 0;
			for(unsigned z=0; z<fz; z++) {
				for(unsigned x=0; x<fx; x++) {
					for(unsigned y=0; y<fy; y++) {
						int val = arr[f][i++];
						fprintf(Fo, "%i,", val);
					}  // Scan Fy
					fprintf(Fo, "\n");
				}  // Scan Fx
			}  // Scan fz
		}  // Scan frames
	}

	if(Foth!=NULL) {
		if(arrth == NULL) {
			printf("Error: Can't generate output thresholds because no input thresholds\n");
			exit(EXIT_FAILURE);
		}
		// Apply scaling
		if(scale != 1) {
			for(unsigned neu=0; neu<n; neu++) {
				for(unsigned i=0; i<2; i++) arrth[neu][i] *= scale;
			}
		}
		// Write output
		for(unsigned neu=0; neu<n; neu++) {
			int val;
			val = ceil(arrth[neu][0]);
			fprintf(Foth, "%i,", val);
			val = floor(arrth[neu][1]);
			fprintf(Foth, "%i,", val);
			fprintf(Foth, "\n");
		}  // neurons
	}

	return 0;
}

